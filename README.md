# dv-runtime

C++ event-based processing framework for neuromorphic cameras, targeting embedded and desktop systems. <br />

Get started documentation: https://inivation.gitlab.io/dv/dv-docs/

# History

The Dynamic Vision (DV) toolkit is the successor to the cAER software, providing a better platform
for development of applications and interaction with event-based sensors, thanks to a better,
simpler API and a first-class graphical user interface (GUI).
The last release of cAER can be retrieved from our version control here:

https://gitlab.com/inivation/dv/dv-runtime/tags/caer-1.1.2

# Dependencies:

NOTE: if you intend to install the latest git master checkout, please also make sure to use the latest
libcaer git master checkout, as we often depend on new features of the libcaer development version.

Linux, MacOS X or Windows (for Windows build instructions see README.Windows) <br />
gcc >= 10.0 or clang >= 11.0 <br />
cmake >= 3.10.0 <br />
Boost >= 1.65 (with system, filesystem, program_options) <br />
OpenSSL (for Boost.ASIO SSL) <br />
OpenCV >= 3.1.0 <br />
libcaer >= 3.3.8 <br />
liblz4 <br />
libzstd <br />
libfmt >= 7.0.3 <br />
Optional: google-perftools >= 2.5 (faster memory allocation, heap/cpu profiling) <br />
Optional: Aravis >= 0.8 (machine vision camera module) <br />
Optional: FFmpeg >= 3.4.0 (live-streaming and video output modules)  <br />
Optional: SFML >= 2.3.0 (deprecated visualizer module, use dv-gui instead) <br />

On Ubuntu Linux: $ sudo apt-get install build-essential cmake pkg-config libboost-all-dev libssl-dev libopencv-dev libopencv-contrib-dev liblz4-dev libzstd-dev libfmt-dev libcaer-dev <br />
On Fedora Linux: $ sudo dnf install @c-development cmake pkgconfig boost-devel openssl-devel opencv-devel lz4-devel libzstd-devel fmt-devel libcaer-devel <br />
On MacOS (using Homebrew): $ brew install cmake pkg-config boost openssl opencv lz4 zstd fmt libcaer <br />

# Installation

1) configure:
<br />
$ cmake -DCMAKE_INSTALL_PREFIX=/usr <OPTIONS> .
<br />
<br />
The following options are currently supported: <br />
-DENABLE_TCMALLOC=1 -- Enable Google-Perftools' TCMalloc memory allocator (plus heap profiler). <br />
-DENABLE_PROFILER=1 -- Enable Google-Perftools' CPU profiler (works best on Linux). <br />
-DENABLE_VISUALIZER=1 -- Open separate windows in which to visualize data (deprecated, use dv-gui instead). <br />
NOTE: heap and CPU profiler can be used together, though the heap profiler will slow down your application and
skew your CPU profiler results (lots of sigprocmask() calls). The CPU profiler is not supported on Windows, and
can give skewed results on MacOS X; it works well on Linux. To get more precise results from the CPU profiler,
you can start dv-runtime manually with the environment variable: CPUPROFILE_FREQUENCY=4000
<br />
2) build:
<br />
$ make
<br />
<br />
3) install:
<br />
$ sudo make install
<br />

# Usage

You will need an XML file that specifies which and how modules
should be interconnected. We recommend using dv-gui to create
the first configuration and interact with it. <br />
Please follow our online documentation and tutorials to get started:
<br />
https://inivation.gitlab.io/dv/dv-docs/
<br />

$ dv-runtime (see online docs for more information) <br />
$ dv-control (command-line run-time control program) <br />

# Paper References

DVS: https://ieeexplore.ieee.org/document/4444573
P. Lichtsteiner, C. Posch and T. Delbruck, "A 128×128 120dB 15us Latency Asynchronous Temporal Contrast Vision Sensor",
IEEE Journal of Solid State Circuits, 43(2) 566-576, 2008.

DAVIS: https://ieeexplore.ieee.org/document/6889103
C. Brandli, R. Berner, M. Yang, S.-C. Liu, and T. Delbruck, “A 240x180 130dB 3us Latency Global Shutter Spatiotemporal Vision Sensor”,
IEEE Journal of Solid State Circuits, 49(10) 2333-2341, 2014.

For an exhaustive list of papers and research concerning event-based vision, please refer to:
https://github.com/uzh-rpg/event-based_vision_resources

# Help

Please use our GitLab bug tracker to report issues and bugs, or
our Google Groups mailing list for discussions and announcements.

BUG TRACKER: https://gitlab.com/inivation/dv/dv-runtime/issues/

MAILING LIST: https://groups.google.com/d/forum/dv-users/

BUILD STATUS: https://gitlab.com/inivation/dv/dv-runtime/pipelines/

CODE ANALYSIS: https://sonarcloud.io/dashboard?id=com.inivation.dv-runtime
