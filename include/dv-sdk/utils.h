#ifndef DV_SDK_UTILS_H_
#define DV_SDK_UTILS_H_

// Suppress unused argument warnings, if needed
#define UNUSED_ARGUMENT(arg) (void) (arg)

// Common includes, useful for everyone.
#ifdef __cplusplus

#	include <cerrno>
#	include <cinttypes>
#	include <cstddef>
#	include <cstdint>
#	include <cstdlib>

#else

#	include <errno.h>
#	include <inttypes.h>
#	include <stdbool.h>
#	include <stddef.h>
#	include <stdint.h>
#	include <stdlib.h>

#endif

// Need to include windows.h first on Windows, as it will redefine
// ERORR, WARNING etc., which we then overwrite below with logLevel.
#if defined(_WIN32) || defined(_WIN64) || defined(__CYGWIN__)
#	define WIN32_LEAN_AND_MEAN
#	include <windows.h>
#endif

#include "config/dvConfig.h"

#ifdef __cplusplus
extern "C" {
#endif

enum dvLogLevel {
	DVLOG_ERROR   = 3,
	DVLOG_WARNING = 4,
	DVLOG_INFO    = 6,
	DVLOG_DEBUG   = 7,
};

LIB_PUBLIC_VISIBILITY void dvLog(enum dvLogLevel level, const char *message);

#ifdef __cplusplus
}

#	include "config/dvConfig.hpp"

#	include <algorithm>
#	include <vector>
#	include <utility>
#	include <memory>
#	include <string>
#	include <stdexcept>
#	include <type_traits>
#	include <fmt/format.h>

namespace dv {

// Undefine the log-level names, to avoid conflicts with macros
// on Windows/MinGW for example.
#	undef ERROR
#	undef WARNING
#	undef INFO
#	undef DEBUG

enum class logLevel {
	ERROR   = DVLOG_ERROR,
	WARNING = DVLOG_WARNING,
	INFO    = DVLOG_INFO,
	DEBUG   = DVLOG_DEBUG,
};

namespace Cfg  = dv::Config;
using CfgType  = dv::Config::AttributeType;
using CfgFlags = dv::Config::AttributeFlags;

template<typename T>
using unique_ptr_deleter = std::unique_ptr<T, void (*)(T *)>;

using unique_ptr_void = unique_ptr_deleter<void>;

template<typename T>
inline unique_ptr_void make_unique_void(T *ptr) {
	return unique_ptr_void(ptr, [](void *data) {
		T *p = static_cast<T *>(data);
		delete p;
	});
}

template<typename T>
inline std::shared_ptr<T> shared_ptr_wrap_extra_deleter(std::shared_ptr<T> in, std::function<void(T *)> action) {
	if (!in) {
		return {};
	}
	if (!action) {
		return in;
	}

	auto new_deleter = [action](std::shared_ptr<T> *p) {
		action(p->get());
		delete p;
	};

	auto tmp = std::shared_ptr<std::shared_ptr<T>>(new std::shared_ptr<T>(std::move(in)), std::move(new_deleter));

	if (!tmp) {
		return {};
	}

	// aliasing constructor:
	return {tmp, tmp.get()->get()};
}

inline void Log(logLevel level, const char *msg) {
	dvLog(static_cast<enum dvLogLevel>(level), msg);
}

inline void Log(logLevel level, const std::string &msg) {
	dvLog(static_cast<enum dvLogLevel>(level), msg.c_str());
}

template<typename S, typename... Args>
inline void Log(logLevel level, const S &format, Args &&...args) {
	dvLog(static_cast<enum dvLogLevel>(level), fmt::format(format, std::forward<Args>(args)...).c_str());
}

template<typename T>
inline bool vectorContains(const std::vector<T> &vec, const T &item) {
	const auto result = std::find(vec.cbegin(), vec.cend(), item);

	if (result == vec.cend()) {
		return (false);
	}

	return (true);
}

template<typename T, typename Pred>
inline bool vectorContainsIf(const std::vector<T> &vec, Pred predicate) {
	const auto result = std::find_if(vec.cbegin(), vec.cend(), predicate);

	if (result == vec.cend()) {
		return (false);
	}

	return (true);
}

template<typename T>
inline void vectorSortUnique(std::vector<T> &vec) {
	std::sort(vec.begin(), vec.end());
	vec.erase(std::unique(vec.begin(), vec.end()), vec.end());
}

template<typename T>
inline bool vectorRemove(std::vector<T> &vec, const T &item) {
	auto firstRemove = std::remove(vec.begin(), vec.end(), item);
	if (firstRemove == vec.end()) {
		return (false); // Nothing to remove.
	}

	vec.erase(firstRemove, vec.end());
	return (true); // Deleted one or more elements.
}

template<typename T, typename Pred>
inline bool vectorRemoveIf(std::vector<T> &vec, Pred predicate) {
	auto firstRemove = std::remove_if(vec.begin(), vec.end(), predicate);
	if (firstRemove == vec.end()) {
		return (false); // Nothing to remove.
	}

	vec.erase(firstRemove, vec.end());
	return (true); // Deleted one or more elements.
}

} // namespace dv

template<typename T>
struct fmt::formatter<std::vector<T>> {
	std::string separator = ", ";

	constexpr auto parse(format_parse_context &ctx) {
		// Parse the presentation format and store it in the formatter:
		auto it = ctx.begin(), end = ctx.end();

		// If there are characters inside the format specifier, use them
		// verbatim as a string separator.
		if ((it != end) && (*it != '}')) {
			separator.clear();
		}

		while ((it != end) && (*it != '}')) {
			separator.push_back(*it);
			it++;
		}

		// Return an iterator past the end of the parsed range:
		return it;
	}

	template<typename FormatContext>
	auto format(const std::vector<T> &vec, FormatContext &ctx) {
		return fmt::format_to(ctx.out(), "{}", fmt::join(vec.cbegin(), vec.cend(), separator));
	}
};

#endif

#endif /* DV_SDK_UTILS_H_ */
