#!/usr/bin/env sh

# exit when any command fails
set -e

mkdir libcaer_build
cd libcaer_build

git clone 'https://gitlab.com/inivation/dv/libcaer.git' .

mkdir build
cd build

cmake -DCMAKE_INSTALL_PREFIX=/usr -DENABLE_OPENCV=1 ..
make -j2 -s
make install

cd ../..
rm -Rf libcaer_build
