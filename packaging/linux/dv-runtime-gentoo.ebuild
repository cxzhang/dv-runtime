# Copyright 2020 iniVation AG
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit eutils cmake-utils

DESCRIPTION="C++ event-based processing framework for neuromorphic cameras, targeting embedded and desktop systems."
HOMEPAGE="https://gitlab.com/inivation/dv/${PN}/"

SRC_URI="https://gitlab.com/inivation/dv/${PN}/-/archive/${PV}/${P}.tar.gz"

LICENSE="AGPL-3"
SLOT="0"
KEYWORDS="amd64 arm x86"
IUSE="debug old_visualizer"

RDEPEND="acct-group/dv-runtime
	acct-user/dv-runtime
	>=dev-libs/libcaer-3.3.9
	>=dev-libs/boost-1.70.0
	dev-libs/openssl
	>=media-libs/opencv-3.2.0
	>=app-arch/lz4-1.8.0
	>=app-arch/zstd-1.3.0
	>=dev-util/google-perftools-2.6
	>=dev-libs/libfmt-7.0.3
	>=media-video/aravis-0.8.2[usb]
	>=media-video/ffmpeg-4.3.1
	old_visualizer? ( >=media-libs/libsfml-2.5.0 x11-libs/libX11 )"

DEPEND="${RDEPEND}
	virtual/pkgconfig
	>=dev-util/cmake-3.10.0"

src_configure() {
	local mycmakeargs=(
		-DENABLE_TCMALLOC=1
		-DENABLE_VISUALIZER="$(usex old_visualizer 1 0)"
	)

	cmake-utils_src_configure
}

src_install() {
	cmake-utils_src_install

	newinitd service/${PN}.openrc ${PN}
}
