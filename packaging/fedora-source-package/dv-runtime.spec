%global __cmake_in_source_build 1

Summary: C++ event-based processing framework for neuromorphic cameras
Name:    dv-runtime
Version: VERSION_REPLACE
Release: 0%{?dist}
License: AGPLv3
URL:     https://gitlab.com/inivation/dv/dv-runtime/
Vendor:  iniVation AG

Source0: https://gitlab.com/inivation/dv/%{name}/-/archive/%{version}/%{name}-%{version}.tar.gz

BuildRequires: gcc >= 10.0, gcc-c++ >= 10.0, systemd-rpm-macros, cmake >= 3.10, pkgconfig >= 0.29.0, libcaer-devel >= 3.3.8, boost-devel >= 1.66, openssl-devel, opencv-devel >= 3.2.0, gperftools-devel >= 2.5, SFML-devel >= 2.3.0, libX11-devel, lz4-devel, libzstd-devel, fmt-devel >= 7.0.3, aravis-devel >= 0.8.2
Requires: libcaer >= 3.3.8, boost >= 1.66, openssl, opencv >= 3.2.0, gperftools >= 2.5, SFML >= 2.3.0, libX11, lz4-libs, libzstd, fmt >= 7.0.3, aravis >= 0.8.2

%description
C++ event-based processing framework for neuromorphic cameras,
targeting embedded and desktop systems.

%package devel
Summary: C++ event-based processing framework for neuromorphic cameras (development files)
Requires: %{name}%{?_isa} = %{version}-%{release}, cmake >= 3.10, pkgconfig >= 0.29.0, libcaer-devel >= 3.3.8, boost-devel >= 1.66, openssl-devel, opencv-devel >= 3.2.0, fmt-devel >= 7.0.3

%description devel
Development files for dv-runtime, such as headers, pkg-config files, etc.,
enabling users to write their own processing modules.

%prep
%autosetup

%build
%cmake -UBUILD_SHARED_LIBS -DENABLE_TCMALLOC=1 -DENABLE_VISUALIZER=1
%cmake_build

%install
%cmake_install

%files
%{_bindir}/dv-runtime
%{_bindir}/dv-control
%{_bindir}/dv-filestat
%{_libdir}/libdvsdk.so.*
%{_datarootdir}/dv/modules/
/lib/systemd/system/dv-runtime.service
%{_sysusersdir}/dv-runtime.sysusers.conf

%files devel
%{_includedir}/dv-sdk/
%{_datarootdir}/dv/dv-modules.cmake
%{_libdir}/libdvsdk.so
%{_libdir}/pkgconfig/
%{_libdir}/cmake/dv/

%changelog
* Mon Apr 20 2020 iniVation AG <support@inivation.com> - VERSION_REPLACE
- See ChangeLog file in source or GitLab.
