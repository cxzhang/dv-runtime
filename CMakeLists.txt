# Init CMake (require at least version 3.10.0)
CMAKE_MINIMUM_REQUIRED(VERSION 3.10.0)

# General build settings
IF (NOT CMAKE_BUILD_TYPE)
	SET(CMAKE_BUILD_TYPE "Release" CACHE STRING "Possible build types: None Debug Release RelWithDebInfo MinSizeRel")
ENDIF()

IF (NOT CMAKE_INSTALL_PREFIX)
	IF (UNIX AND NOT APPLE)
		SET(CMAKE_INSTALL_PREFIX "/usr" CACHE STRING "CMake default install prefix, set to /usr on Unix/Linux")
	ELSEIF (APPLE)
		SET(CMAKE_INSTALL_PREFIX "/usr/local" CACHE STRING "CMake default install prefix, set to /usr/local on macOS")
	ELSE()
		MESSAGE(FATAL_ERROR "CMAKE_INSTALL_PREFIX is not set")
	ENDIF()
ENDIF()

IF (NOT ENABLE_SANITIZER)
	SET(ENABLE_SANITIZER "None" CACHE STRING "Possible sanitizers: None, Address, Memory[Only with Clang], Undefined, Thread.")
ENDIF()

IF (NOT ENABLE_STATIC_ANALYSIS)
	SET(ENABLE_STATIC_ANALYSIS "None" CACHE STRING "Possible static analysis tools: clang-tidy, iwyu, cppcheck.")
ENDIF()

# Project name and version
PROJECT(dv-runtime
	VERSION 1.4.4
	LANGUAGES C CXX)
SET(CMAKE_C_STANDARD 11)
SET(CMAKE_C_STANDARD_REQUIRED ON)
SET(CMAKE_CXX_STANDARD 17)
SET(CMAKE_CXX_STANDARD_REQUIRED ON)

ADD_DEFINITIONS(-DDV_PROJECT_VERSION="${PROJECT_VERSION}")

# Define installation paths
INCLUDE(GNUInstallDirs)

# Custom CMake base module
SET(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH}
	${CMAKE_SOURCE_DIR}/cmakemod
	${CMAKE_INSTALL_PREFIX}/${CMAKE_INSTALL_DATAROOTDIR}/caer ${CMAKE_INSTALL_PREFIX}/share/caer
	/usr/${CMAKE_INSTALL_DATAROOTDIR}/caer /usr/share/caer
	/usr/local/${CMAKE_INSTALL_DATAROOTDIR}/caer /usr/local/share/caer)

# Support new Homebrew paths on MacOS
IF (APPLE)
	SET(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH}
		/opt/homebrew/${CMAKE_INSTALL_DATAROOTDIR}/caer /opt/homebrew/share/caer)
ENDIF()

# Basic setup for DV-runtime (uses libcaer)
INCLUDE(caer-base)
CAER_SETUP()

# Override RPATH settings
SET(CMAKE_MACOSX_RPATH TRUE)
SET(CMAKE_SKIP_BUILD_RPATH FALSE)
SET(CMAKE_BUILD_WITH_INSTALL_RPATH FALSE)
SET(CMAKE_INSTALL_RPATH_USE_LINK_PATH TRUE)

# Setup atomics.
INCLUDE(CheckAtomic)

# Enable shared library unloading on dlclose() with GCC
SET(CMAKE_C_VISIBILITY_PRESET hidden)
SET(CMAKE_CXX_VISIBILITY_PRESET hidden)
SET(CMAKE_VISIBILITY_INLINES_HIDDEN 1)

# libcaer devices and logging support.
FIND_PACKAGE(libcaer 3.3.8 REQUIRED)

# libfmt support for C++ string formatting.
FIND_PACKAGE(fmt 7.0.3 REQUIRED)

SET(DV_PKGCONFIG_REQUIRES_PRIVATE "fmt >= 7.0.3")

# Boost.nowide for UTF-8 cross-platform path/files support.
FIND_PACKAGE(Boost 1.56 COMPONENTS nowide)
IF (NOT Boost_NOWIDE_FOUND)
	MESSAGE(STATUS "Using internal Boost.nowide")
	ADD_SUBDIRECTORY(nowide)
ENDIF()

# Boost support for C++
FIND_PACKAGE(Boost 1.56 REQUIRED COMPONENTS system filesystem program_options)

# Fix new Boost/CMake behavior that doesn't properly declare the versions
IF (NOT DEFINED Boost_MAJOR_VERSION)
	SET(Boost_MAJOR_VERSION ${Boost_VERSION_MAJOR})
	SET(Boost_MINOR_VERSION ${Boost_VERSION_MINOR})
	SET(Boost_SUBMINOR_VERSION ${Boost_VERSION_PATCH})
ENDIF()

# boost.ASIO needs threading
SET(BOOST_ASIO_LIBRARIES ${SYSTEM_THREAD_LIBS})

# Windows boost.ASIO needs extra libraries
IF (OS_WINDOWS)
	SET(BOOST_ASIO_LIBRARIES ${BOOST_ASIO_LIBRARIES} wsock32 ws2_32)
ENDIF()

# boost.ASIO SSL needs OpenSSL too
# Use -DOPENSSL_ROOT_DIR=x to select a specific installation.
FIND_PACKAGE(OpenSSL REQUIRED)

SET(BOOST_ASIO_LIBRARIES ${BOOST_ASIO_LIBRARIES} OpenSSL::SSL OpenSSL::Crypto)

# OpenCV support.
FIND_PACKAGE(OpenCV REQUIRED COMPONENTS core imgproc)
IF (OpenCV_VERSION VERSION_LESS "3.1.0")
	MESSAGE(FATAL_ERROR "Cannot find OpenCV 3.1.0 or newer.")
ENDIF()

IF (OS_WINDOWS)
	# Generation of RC file for windows
	SET(WINDOWS_RC_FILE packaging/windows/windows.rc)
	SET(WINDOWS_OBJ_FILE packaging/windows/windows.obj)

	STRING(REGEX REPLACE "[^0-9]" "," RC_PRODUCT_VERSION ${PROJECT_VERSION})
	CONFIGURE_FILE(${WINDOWS_RC_FILE}.in ${WINDOWS_RC_FILE} @ONLY)

	SET(CMAKE_RC_COMPILER_INIT windres)
	ENABLE_LANGUAGE(RC)
	SET(CMAKE_RC_COMPILE_OBJECT "<CMAKE_RC_COMPILER> <FLAGS> -O coff <DEFINES> -i <SOURCE> -o <OBJECT>")
	MESSAGE(STATUS "Windres executable: ${CMAKE_RC_COMPILER}")

	ADD_CUSTOM_TARGET(rc ALL
		COMMAND ${CMAKE_RC_COMPILER} -i ${WINDOWS_RC_FILE} -o ${WINDOWS_OBJ_FILE})
ENDIF()

# Define module installation paths.
SET(DV_MODULES_DIR ${CMAKE_INSTALL_DATAROOTDIR}/dv/modules)
MESSAGE(STATUS "Final modules installation directory is: ${CMAKE_INSTALL_PREFIX}/${DV_MODULES_DIR}")
ADD_DEFINITIONS(-DDV_MODULES_DIR=${USER_LOCAL_PREFIX}/${DV_MODULES_DIR})

# Static analysis tools support
IF (NOT ENABLE_STATIC_ANALYSIS STREQUAL "None")
	MESSAGE(STATUS "Enabling static analysis tool: ${ENABLE_STATIC_ANALYSIS}")

	IF (ENABLE_STATIC_ANALYSIS STREQUAL "clang-tidy")
		FIND_PROGRAM(CLANG_TIDY_PATH NAMES clang-tidy)

		IF (CLANG_TIDY_PATH)
			SET(CMAKE_C_CLANG_TIDY ${CLANG_TIDY_PATH})
			SET(CMAKE_CXX_CLANG_TIDY ${CLANG_TIDY_PATH})
		ENDIF()
	ENDIF()

	IF (ENABLE_STATIC_ANALYSIS STREQUAL "cppcheck")
		FIND_PROGRAM(CPPCHECK_PATH NAMES cppcheck)

		IF (CPPCHECK_PATH)
			SET(CMAKE_C_CPPCHECK ${CPPCHECK_PATH})
			SET(CMAKE_CXX_CPPCHECK ${CPPCHECK_PATH})
		ENDIF()
	ENDIF()

	IF (ENABLE_STATIC_ANALYSIS STREQUAL "iwyu")
		FIND_PROGRAM(IWYU_PATH NAMES include-what-you-use iwyu)

		IF (IWYU_PATH)
			SET(CMAKE_C_INCLUDE_WHAT_YOU_USE ${IWYU_PATH})
			SET(CMAKE_CXX_INCLUDE_WHAT_YOU_USE ${IWYU_PATH})
		ENDIF()
	ENDIF()
ENDIF()

# Sanitizers and testing support
SET(TEST_FLAGS "")

IF (NOT ENABLE_SANITIZER STREQUAL "None")
	MESSAGE(STATUS "Enabling sanitizer: ${ENABLE_SANITIZER}")

	IF (ENABLE_SANITIZER STREQUAL "Address")
		SET(TEST_FLAGS "ASAN_OPTIONS=detect_stack_use_after_return=1:intercept_tls_get_addr=0")
		SET(SANITIZER_FLAGS "-fno-common -fsanitize=address -fsanitize-address-use-after-scope")
	ENDIF()

	IF (ENABLE_SANITIZER STREQUAL "Memory" AND CC_CLANG)
		SET(TEST_FLAGS "MSAN_OPTIONS=poison_in_dtor=1")
		SET(SANITIZER_FLAGS "-fsanitize=memory -fsanitize-memory-track-origins -fsanitize-memory-use-after-dtor")
	ENDIF()

	IF (ENABLE_SANITIZER STREQUAL "Undefined")
		SET(TEST_FLAGS "UBSAN_OPTIONS=print_stacktrace=1,suppressions=${CMAKE_SOURCE_DIR}/docs/undefined_sanitizer_suppress.txt")

		IF (CC_CLANG)
			SET(SANITIZER_FLAGS "-fsanitize=undefined,float-divide-by-zero,implicit-conversion,local-bounds")
		ELSE()
			SET(SANITIZER_FLAGS "-fsanitize=undefined,float-divide-by-zero,float-cast-overflow")
		ENDIF()
	ENDIF()

	IF (ENABLE_SANITIZER STREQUAL "Thread")
		SET(TEST_FLAGS "TSAN_OPTIONS=detect_deadlocks=1,second_deadlock_stack=1,suppressions=${CMAKE_SOURCE_DIR}/docs/thread_sanitizer_suppress.txt")
		SET(SANITIZER_FLAGS "-fsanitize=thread")
	ENDIF()

	ADD_COMPILE_DEFINITIONS(DISABLE_SHARED_UNLOAD)
	SET(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${SANITIZER_FLAGS}")
	SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${SANITIZER_FLAGS}")
	SET(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} ${SANITIZER_FLAGS}")
	SET(CMAKE_SHARED_LINKER_FLAGS "${CMAKE_SHARED_LINKER_FLAGS} ${SANITIZER_FLAGS}")

	# Enable minimal optimization to get acceptable performance in debug mode
	SET(CMAKE_C_FLAGS_DEBUG "${CMAKE_C_FLAGS_DEBUG} -O1 -fno-omit-frame-pointer ")
	SET(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -O1 -fno-omit-frame-pointer ")
ENDIF()

IF (OS_WINDOWS)
	# Set common output directory for build results, to allow testing to find the proper libraries (libdvsdk)
	SET(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/all_bins/)

	ADD_CUSTOM_TARGET(tests COMMAND
		python3 -u "${CMAKE_SOURCE_DIR}/automated_test/runtime-test.py"
		--runtime-exec="${CMAKE_BINARY_DIR}/all_bins/dv-runtime.exe"
		--modules-path="${CMAKE_BINARY_DIR}/all_bins/modules/"
		--control-exec="${CMAKE_BINARY_DIR}/all_bins/dv-control.exe"
		--env-vars="${TEST_FLAGS}"
		--tests=\$\{TESTS\}
		--perf-data-append=\$\{PERF_DATA_APPEND\}
		USES_TERMINAL)
ELSE()
	ADD_CUSTOM_TARGET(tests COMMAND
		python3 -u "${CMAKE_SOURCE_DIR}/automated_test/runtime-test.py"
		--runtime-exec="${CMAKE_BINARY_DIR}/src/dv-runtime"
		--modules-path="${CMAKE_BINARY_DIR}/modules/"
		--control-exec="${CMAKE_BINARY_DIR}/utils/dv-control/dv-control"
		--env-vars="${TEST_FLAGS}"
		--tests=\$\{TESTS\}
		--perf-data-append=\$\{PERF_DATA_APPEND\}
		USES_TERMINAL)
ENDIF()

# Add main include dir for all targets.
INCLUDE_DIRECTORIES(${CMAKE_SOURCE_DIR}/include/)

# Install header files.
ADD_SUBDIRECTORY(include)

# Compile libdvsdk and dv-runtime main executable.
ADD_SUBDIRECTORY(src)

# Compile extra modules and utilities.
ADD_SUBDIRECTORY(modules)
ADD_SUBDIRECTORY(utils)

# Generate pkg-config file
CONFIGURE_FILE(libdvsdk.pc.in libdvsdk.pc @ONLY)

INSTALL(FILES ${CMAKE_BINARY_DIR}/libdvsdk.pc DESTINATION ${CMAKE_INSTALL_LIBDIR}/pkgconfig)

# Export the cmake configuration for the install tree
INCLUDE(CMakePackageConfigHelpers)
WRITE_BASIC_PACKAGE_VERSION_FILE(
	${PROJECT_BINARY_DIR}/dvConfigVersion.cmake
	VERSION ${PROJECT_VERSION}
	COMPATIBILITY SameMajorVersion)
SET(include_dirs ${CMAKE_INSTALL_INCLUDEDIR})
SET(export_destination ${CMAKE_INSTALL_LIBDIR}/cmake/dv)
CONFIGURE_PACKAGE_CONFIG_FILE(
	${PROJECT_SOURCE_DIR}/cmakemod/dvConfig.cmake.in
	${PROJECT_BINARY_DIR}/dvConfig.cmake
	INSTALL_DESTINATION ${export_destination}
	PATH_VARS include_dirs export_destination)
INSTALL(EXPORT dv-exports
	NAMESPACE dv::
	DESTINATION ${export_destination})
INSTALL(FILES
	${PROJECT_BINARY_DIR}/dvConfigVersion.cmake
	${PROJECT_BINARY_DIR}/dvConfig.cmake
	DESTINATION ${export_destination})
UNSET(include_dirs)
UNSET(export_destination)

# Install CMake file for modules
INSTALL(FILES cmakemod/dv-modules.cmake DESTINATION ${CMAKE_INSTALL_DATAROOTDIR}/dv)

# Install systemd unit file
IF (OS_LINUX)
	INSTALL(FILES service/dv-runtime.service DESTINATION "/lib/systemd/system/")
	INSTALL(FILES service/dv-runtime.sysusers.conf DESTINATION "/usr/lib/sysusers.d/")
ENDIF()

# Support automatic RPM generation
SET(CPACK_PACKAGE_NAME ${PROJECT_NAME})
SET(CPACK_PACKAGE_VERSION ${PROJECT_VERSION})
SET(CPACK_PACKAGE_RELEASE 1)
SET(CPACK_PACKAGE_CONTACT "support@inivation.com")
SET(CPACK_PACKAGE_VENDOR "iniVation AG")
SET(CPACK_PACKAGE_DESCRIPTION "C++ event-based processing framework for neuromorphic cameras, targeting embedded and desktop systems.")
SET(CPACK_PACKAGE_DESCRIPTION_SUMMARY ${CPACK_PACKAGE_DESCRIPTION})
SET(CPACK_PACKAGING_INSTALL_PREFIX ${CMAKE_INSTALL_PREFIX})
SET(CPACK_PACKAGE_FILE_NAME "${CPACK_PACKAGE_NAME}.${CMAKE_SYSTEM_PROCESSOR}")
SET(CPACK_GENERATOR "RPM" "DEB")
SET(CPACK_RPM_PACKAGE_AUTOREQ 1)
SET(CPACK_RPM_PACKAGE_REQUIRES "cmake >= 3.10, boost-devel >= 1.62, openssl-devel, opencv-devel >= 3.2.0")
SET(CPACK_DEBIAN_PACKAGE_SHLIBDEPS 1)
SET(CPACK_DEBIAN_PACKAGE_DEPENDS "cmake (>= 3.10), libboost-all-dev (>= 1.62), libssl-dev, libopencv-dev (>= 3.2.0)")

INCLUDE(CPack)
