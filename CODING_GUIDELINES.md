# dv-runtime Coding Guidelines

## Code structure

- Each class must be separated into `.cpp` and `.hpp` files.
- Every symbol that is used in the `.cpp` file must be declared in the `.hpp` or files that are included. No local declarations in `.cpp` files.
- Declarations in the `.hpp` file should be ordered by accessibility modifer: `public`, `protected` and `private` (in this order).
- Each section above is then further divided in the following order:
    1. `static` variables
    1. `static` functions
    1. other variables
    1. constructors
    1. destructors
    1. other functions, in decreasing level of abstraction.
- Functions should be located above other functions that they depend on.
- "Your code should read like an article" - Uncle Bob in *[Clean Code](https://www.oreilly.com/library/view/clean-code/9780136083238/)*

## Code formatting

For code formatting we use `clang-format` or `yapf`.

## Code guidelines

- All control blocks have brackets around them: if () { ... }, while () { ... } - one line statements without brackets are forbidden.
- Limit the depth of your ifs and other nested control blocks. Instead of an ```if (x) { ... }``` that spans your entire 50-lines function, prefer
  ```if (!x) { return; }``` to return early. In general when you've hit 5 levels of indentation, consider refactoring.
- Put parentheses `()` inside complex if statement expressions: ```if (((a - b) > c) && !d && (e == f.x) && ((func(y) == 42) || !fonc(z))) {}```
- Newlines are your friend. Sprinkle them around to break up code blocks. Really, they have no drawbacks!
- Dead code should never be committed (unused functions and variables).
- Use structs/classes to keep related data together.
- Use `auto` for local variable types wherever possible in C++ (`var` in Java).
- Generally don't pass complex objects by value in function parameters. This results in a full copy and thus bad performance.
  If you need to modify the parameter inside the function and have the effects propagate outside, pass a reference (`&`) or a pointer (`*`).
  If you don't need to modify it, pass by const-reference (`const &`). This is often the case for strings.
  Only if you need to modify it BUT not have the change propagate outside can it make sense to pass by value, though passing by reference and
  then making a local copy is usually preferred, as it signals your intent clearly.
- Cleanup your header inclusions and put them in the most specific file possible (the corresponding `.cpp` if it's the only place using it).

## Error handling

- Functions must not return `null` or error codes, exceptions must be thrown in case of errors
- Error handling code should be separated from code for normal processing (no nested `if`/`else` statements checking for and dealing with errors)

## Libraries

- file operations: boost::filesystem (IMPORTANT: use this instead of std::filesystem due to simpler library dependencies!)
- better error handling: boost::exceptions
- printf-style printing in C++: fmt::format
- networking: boost::asio
- string splitting on token (eg. CSV): boost::tokenizer
- simple string split/join: boost::algorithm::string

## Documentation

Public interfaces should be documented using Doxygen-compatible comments syntax:

- https://github.com/stan-dev/stan/wiki/How-to-Write-Doxygen-Doc-Comments
- http://www.doxygen.nl/index.html

## Comments

Code comments should use the `//` style. Long multi-line comments (4+ lines) can use the `/* */` style, but if you find yourself writing a gigantic
comment to explain things in the middle of the code, maybe split that code out into its own function and use it as exhaustive interface documentation.

# Naming conventions

## Function naming

CamelCase should be preferred, though underscore_notation is also allowed, the important thing is to be consistent within a source-file and/or module, and if possible within an entire project.

Function names should be pronounceable, meaningful, and generally longer than four characters. They should ideally read like a title for a book chapter, where the function's code is the chapter's content.

## Variable naming

We do not use type encodings, such as hungarian notation, or member prefixes and suffixes in our code. This only causes confusion and decreases readability. IDEs take care of this for us.

CamelCase should be preferred, though underscore_notation is also allowed, the important thing is to be consistent within a source-file and/or module, and if possible within an entire project.

Variable names should be pronounceable, meaningful, and generally longer than two characters (except for loop variables of course).

## File naming

Files should have useful names, containing their purpose. Always add a proper file extension.
File names should consist entirely of the a-z A-Z 0-9 - and _ characters to be truly portable.
Dots are generally also allowed, though usage of only one to separate the extension is preferred.

A simple C++ regex to sanitize a file name:
```
#include <regex>
const std::regex filenameCleanupRegex{"[^a-zA-Z-_\\d]"};
auto cleanFilename = std::regex_replace(originalFilename, filenameCleanupRegex, "_");
```

# Other

- When loading data from file, or network, always check, check, check! Never assume data is present or makes sense, check everything you can!
  Lengths, formats, valid characters, sequence of operations, ... OpenCV for example silently returns empty/zero objects when using cv::FileStorage,
  so to actually know if there were values in the loaded file, you have to compare the returned object's `.type()` to `cv::FileNode::NONE`.
- Try to fix as many warnings as possible, especially unused arguments often indicate an issue (either not used by mistake, or bad API).
  Also integer comparisons of different sizes or sign can often be easily fixed by casting or using the appropriate unsigned types (`size_t`),
  but this indicates you've at least thought about the issue and what values your integers can take. Dead/unreachable code is never allowed.
- Check your grammar, typos in code, comments, log messages or documentation should be avoided. Most IDEs at least spell check comments
  and strings in English automatically or have plugins (QtCreator: https://github.com/CJCombrink/SpellChecker-Plugin/).
- C++ widely uses the RAII (Resource Acquisition Is Initialization) paradigm, so generally objects clean up after themselves when they go
  out of scope and their destructor is called. Calling file.close() or similar is often not needed, especially for temporary objects.
- Integer flags should be added together via or '|' and not addition '+'. Addition only works if all flags are exact powers of two,
  which is not guaranteed (flags may imply other flags).
- std::string can be directly compared, ie.: ```if (str == "hello") {}```

# DV SDK specific

- `dv::Frame::FrameFormat` is OpenCV-compatible, the values behind the enum correspond to OpenCV frame format values.
- Make sure that all class member variables are actually used somewhere. The compiler can't always analyze this. Same for configuration
  options (`dv::ConfigOption`), the compiler can't verify these at all.
- Finding the home directory of the current user is not simple or portable, use the function `dv::portable_get_user_home_directory()`
  from <dv-sdk/cross/portable_io.h>, it will try to find the real home, or in absence of one, the system temporary directory, and check
  that it exists and is actually a directory. On failure it will throw an exception. Don't use `boost::filesystem::current_path()`.
- `dv::cvector` (from <dv-sdk/data/cvector.hpp>) provides a nice std::vector alternative, that is low-level C-compatible across multiple
  compilers and standard libraries, can convert to/from std::vector, and adds some useful features like append() and addition of vectors.
