#ifndef STEREO_RECTIFICATION_EVENTS_HPP
#define STEREO_RECTIFICATION_EVENTS_HPP

#include "dv-sdk/data/event.hpp"

#include "stereo_rectification.hpp"

class StereoRectificationEvents : public StereoRectification {
public:
	static const char *initDescription();
	static void initInputs(dv::InputDefinitionList &in);
	static void initOutputs(dv::OutputDefinitionList &out);

	StereoRectificationEvents();

	void run() override;

private:
	std::vector<cv::Point2i> remapEvents[NUM_CAMERAS];

	void initStereoRectification(
		const CameraInfo loaded[NUM_CAMERAS], const cv::Mat R, const cv::Mat T, const double alpha) override;
	void stereoRectifyEvents(const dv::InputVectorDataWrapper<dv::EventPacket, dv::Event> &in,
		dv::OutputVectorDataWrapper<dv::EventPacket, dv::Event> &out, size_t cam);
};

#endif // STEREO_RECTIFICATION_EVENTS_HPP
