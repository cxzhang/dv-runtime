#define DV_API_OPENCV_SUPPORT 0

#include <libcaercpp/devices/device_discover.hpp>
#include <libcaercpp/devices/samsung_evk.hpp>

#include "dv-sdk/data/event.hpp"
#include "dv-sdk/module.hpp"

#include "aedat4_convert.hpp"

#include <array>
#include <chrono>
#include <vector>

void *dvModuleGetHooks(enum dvModuleHooks hook) {
	// Support only device discovery.
	if (hook != DV_HOOK_DEVICE_DISCOVERY) {
		return nullptr;
	}

	// Get current device status via libcaer.
	// Suppress logging of libusb errors.
	std::vector<struct caer_device_discovery_result> devices;

	try {
		libcaer::log::disable(true);

		devices = libcaer::devices::discover::device(CAER_DEVICE_SAMSUNG_EVK);

		libcaer::log::disable(false);
	}
	catch (const std::exception &ex) {
		dv::Log(dv::logLevel::ERROR, "Device Discovery (Samsung EVK): '{:s} :: {:s}'.",
			boost::core::demangle(typeid(ex).name()), ex.what());
		return nullptr;
	}

	// Add devices to config tree.
	auto devicesNode = dv::Cfg::GLOBAL.getNode("/system/_devices/");

	for (const auto &dev : devices) {
		const struct caer_samsung_evk_info *info = &dev.deviceInfo.samsungEVKInfo;

		const auto nodeName = fmt::format(FMT_STRING("samsung_evk_{:d}-{:d}/"),
			static_cast<int>(info->deviceUSBBusNumber), static_cast<int>(info->deviceUSBDeviceAddress));

		auto devNode = devicesNode.getRelativeNode(nodeName);

		devNode.create<dv::CfgType::STRING>("OpenWithModule", "dv_samsung_evk", {1, 32},
			dv::CfgFlags::READ_ONLY | dv::CfgFlags::NO_EXPORT, "Open device with specified module.");

		devNode.create<dv::CfgType::INT>("USBBusNumber", info->deviceUSBBusNumber, {0, 255},
			dv::CfgFlags::READ_ONLY | dv::CfgFlags::NO_EXPORT, "USB bus number.");
		devNode.create<dv::CfgType::INT>("USBDeviceAddress", info->deviceUSBDeviceAddress, {0, 255},
			dv::CfgFlags::READ_ONLY | dv::CfgFlags::NO_EXPORT, "USB device address.");

		if (!dev.deviceErrorOpen) {
			devNode.create<dv::CfgType::STRING>("SerialNumber", info->deviceSerialNumber, {0, 8},
				dv::CfgFlags::READ_ONLY | dv::CfgFlags::NO_EXPORT, "USB device serial number.");
			devNode.create<dv::CfgType::INT>("FirmwareVersion", info->firmwareVersion, {0, INT16_MAX},
				dv::CfgFlags::READ_ONLY | dv::CfgFlags::NO_EXPORT, "Version of device firmware.");

			if (!dev.deviceErrorVersion) {
				devNode.create<dv::CfgType::INT>("DVSSizeX", info->dvsSizeX, {0, INT16_MAX},
					dv::CfgFlags::READ_ONLY | dv::CfgFlags::NO_EXPORT, "DVS X axis resolution.");
				devNode.create<dv::CfgType::INT>("DVSSizeY", info->dvsSizeY, {0, INT16_MAX},
					dv::CfgFlags::READ_ONLY | dv::CfgFlags::NO_EXPORT, "DVS Y axis resolution.");
			}
		}
	}

	// Nothing to return, no further steps needed.
	return nullptr;
}

class samsungEVK : public dv::ModuleBase {
private:
	libcaer::devices::samsungEVK device;

public:
	static void initOutputs(dv::OutputDefinitionList &out) {
		out.addEventOutput("events");
	}

	static const char *initDescription() {
		return ("Samsung EVK camera support.");
	}

	static void initConfigOptions(dv::RuntimeConfig &config) {
		config.add("busNumber", dv::ConfigOption::intOption("USB bus number restriction.", 0, 0, UINT8_MAX));
		config.add("devAddress", dv::ConfigOption::intOption("USB device address restriction.", 0, 0, UINT8_MAX));
		config.add("serialNumber", dv::ConfigOption::stringOption("USB serial number restriction.", ""));

		config.add("eventsFlatten", dv::ConfigOption::boolOption("Flatten events to all be ON events."));
		config.add("eventsOnOnly", dv::ConfigOption::boolOption("Only generate ON events."));
		config.add("eventsOffOnly", dv::ConfigOption::boolOption("Only generate OFF events."));

		config.add("subsample", dv::ConfigOption::boolOption("Enable sub-sampling of events."));
		config.add("subsampleHorizontal",
			dv::ConfigOption::listOption("Horizontal sub-sampling factor.", 0, {"none", "1/2", "1/4", "1/8"}));
		config.add("subsampleVertical",
			dv::ConfigOption::listOption("Vertical sub-sampling factor.", 0, {"none", "1/2", "1/4", "1/8"}));
		config.add("dualBinning", dv::ConfigOption::boolOption("Enable dual-binning of events."));

		// TODO: add area blocking.
		// config.add("areaBlocking", dv::ConfigOption::boolOption("Enable blocking off 32x32 patches of events."));
		// config.add("areaBlockingSelection", dv::ConfigOption::stringOption("Block off 32x32 patches of
		// events.","0000"));

		config.add("externalTriggerMode", dv::ConfigOption::listOption("External trigger reaction.", 0,
											  {"Reset Timestamps", "Single Frame Readout"}));

		config.add("globalReset", dv::ConfigOption::boolOption("Enable global reset."));
		config.add("globalResetDuringReadout",
			dv::ConfigOption::boolOption("Enable global reset during readout of event frames."));
		config.add("globalHold", dv::ConfigOption::boolOption("Enable global hold.", true));
		config.add("fixedReadTime", dv::ConfigOption::boolOption("Enable fixed time event frame readout."));

		// TODO: add timing options.

		config.add(
			"activityMonitor", dv::ConfigOption::boolOption("Only send events if there is activity in the scene."));
		config.add(
			"activityPositiveThreshold", dv::ConfigOption::intOption("Positive event threshold.", 300, 1, 65535));
		config.add("activityNegativeThreshold", dv::ConfigOption::intOption("Negative event threshold.", 20, 1, 65535));
		config.add("activityDecrementRate", dv::ConfigOption::intOption("Decrement number.", 1, 1, 255));
		config.add("activityDecrementTime", dv::ConfigOption::intOption("Decrement time.", 3, 1, 255));
		config.add("activityPositiveMaxValue", dv::ConfigOption::intOption("Positive maximum value.", 300, 1, 65535));

		// Simplified bias control.
		config.add("biasSensitivity",
			dv::ConfigOption::listOption("Configure contrast sensitivity via predefined values for current biases.", 2,
				{"Very Low", "Low", "Default", "High", "Very High"}));

		config.setPriorityOptions({"biasSensitivity"});
	}

	static void moduleShutdownNotify(void *p) {
		dv::Cfg::Node moduleNode = static_cast<dvConfigNode>(p);

		// Ensure parent also shuts down (on disconnected device for example).
		moduleNode.putBool("running", false);
	}

	samsungEVK() :
		device(0, static_cast<uint8_t>(config.getInt("busNumber")), static_cast<uint8_t>(config.getInt("devAddress")),
			config.getString("serialNumber")) {
		auto devInfo = device.infoGet();

		// Add size-dependant configuration.
		config.add("crop", dv::ConfigOption::boolOption("Enable Region of Interest (ROI)."));
		config.add("cropStartX",
			dv::ConfigOption::intOption("Horizontal (X axis) start of ROI.", 0, 0, device.infoGet().dvsSizeX - 1));
		config.add("cropStartY",
			dv::ConfigOption::intOption("Vertical (Y axis) start of ROI.", 0, 0, device.infoGet().dvsSizeY - 1));
		config.add("cropEndX", dv::ConfigOption::intOption("Horizontal (X axis) end of ROI.",
								   device.infoGet().dvsSizeX - 1, 0, device.infoGet().dvsSizeX - 1));
		config.add("cropEndY", dv::ConfigOption::intOption("Vertical (Y axis) end of ROI.",
								   device.infoGet().dvsSizeY - 1, 0, device.infoGet().dvsSizeY - 1));

		// Generate source string for output modules.
		auto sourceString = std::string("Samsung_EVK_") + devInfo.deviceSerialNumber;

		// Setup outputs.
		outputs.getEventOutput("events").setup(device.infoGet().dvsSizeX, device.infoGet().dvsSizeY, sourceString);

		auto sourceInfoNode = moduleNode.getRelativeNode("sourceInfo/");

		sourceInfoNode.create<dv::CfgType::STRING>("serialNumber", devInfo.deviceSerialNumber, {0, 8},
			dv::CfgFlags::READ_ONLY | dv::CfgFlags::NO_EXPORT, "Device serial number.");
		sourceInfoNode.create<dv::CfgType::INT>("usbBusNumber", devInfo.deviceUSBBusNumber, {0, 255},
			dv::CfgFlags::READ_ONLY | dv::CfgFlags::NO_EXPORT, "Device USB bus number.");
		sourceInfoNode.create<dv::CfgType::INT>("usbDeviceAddress", devInfo.deviceUSBDeviceAddress, {0, 255},
			dv::CfgFlags::READ_ONLY | dv::CfgFlags::NO_EXPORT, "Device USB device address.");

		sourceInfoNode.create<dv::CfgType::INT>("firmwareVersion", devInfo.firmwareVersion,
			{devInfo.firmwareVersion, devInfo.firmwareVersion}, dv::CfgFlags::READ_ONLY | dv::CfgFlags::NO_EXPORT,
			"Device USB firmware version.");
		sourceInfoNode.create<dv::CfgType::INT>("chipID", devInfo.chipID, {devInfo.chipID, devInfo.chipID},
			dv::CfgFlags::READ_ONLY | dv::CfgFlags::NO_EXPORT, "Device chip identification number.");

		sourceInfoNode.create<dv::CfgType::STRING>("source", sourceString,
			{static_cast<int32_t>(sourceString.length()), static_cast<int32_t>(sourceString.length())},
			dv::CfgFlags::READ_ONLY | dv::CfgFlags::NO_EXPORT, "Device source information.");

		// Ensure good defaults for data acquisition settings.
		// No blocking behavior due to mainloop notification, and no auto-start of
		// all producers to ensure cAER settings are respected.
		device.configSet(CAER_HOST_CONFIG_DATAEXCHANGE, CAER_HOST_CONFIG_DATAEXCHANGE_BLOCKING, true);
		device.configSet(CAER_HOST_CONFIG_DATAEXCHANGE, CAER_HOST_CONFIG_DATAEXCHANGE_START_PRODUCERS, true);
		device.configSet(CAER_HOST_CONFIG_DATAEXCHANGE, CAER_HOST_CONFIG_DATAEXCHANGE_STOP_PRODUCERS, true);

		// Create default settings and send them to the device.
		device.sendDefaultConfig();

		// Set timestamp offset for real-time timestamps. DataStart() will
		// reset the device-side timestamp.
		int64_t tsNowOffset
			= std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch())
				  .count();

		sourceInfoNode.create<dv::CfgType::LONG>("tsOffset", tsNowOffset, {0, INT64_MAX},
			dv::CfgFlags::READ_ONLY | dv::CfgFlags::NO_EXPORT,
			"Time offset of data stream starting point to Unix time in µs.");

		moduleNode.getRelativeNode("outputs/events/info/")
			.create<dv::CfgType::LONG>("tsOffset", tsNowOffset, {0, INT64_MAX},
				dv::CfgFlags::READ_ONLY | dv::CfgFlags::NO_EXPORT,
				"Time offset of data stream starting point to Unix time in µs.");

		// Start data acquisition.
		device.dataStart(nullptr, nullptr, nullptr, &moduleShutdownNotify, moduleData->moduleNode);
	}

	~samsungEVK() override {
		device.dataStop();

		// Clear sourceInfo node.
		auto sourceInfoNode = moduleNode.getRelativeNode("sourceInfo/");
		sourceInfoNode.removeAllAttributes();
	}

	void run() override {
		auto data = device.dataGet();

		if (!data || data->empty()) {
			return;
		}

		if (data->getEventPacket(POLARITY_EVENT)) {
			dvConvertToAedat4(data->getEventPacket(POLARITY_EVENT)->getHeaderPointer(), moduleData);
		}
	}

	void configUpdate() override {
		device.configSet(SAMSUNG_EVK_DVS, SAMSUNG_EVK_DVS_EVENT_FLATTEN, config.getBool("eventsFlatten"));
		device.configSet(SAMSUNG_EVK_DVS, SAMSUNG_EVK_DVS_EVENT_ON_ONLY, config.getBool("eventsOnOnly"));
		device.configSet(SAMSUNG_EVK_DVS, SAMSUNG_EVK_DVS_EVENT_OFF_ONLY, config.getBool("eventsOffOnly"));

		device.configSet(SAMSUNG_EVK_DVS, SAMSUNG_EVK_DVS_SUBSAMPLE_ENABLE, config.getBool("subsample"));

		auto subsampleHorizontal = config.getString("subsampleHorizontal");
		if (subsampleHorizontal == "1/2") {
			device.configSet(
				SAMSUNG_EVK_DVS, SAMSUNG_EVK_DVS_SUBSAMPLE_HORIZONTAL, SAMSUNG_EVK_DVS_SUBSAMPLE_HORIZONTAL_HALF);
		}
		else if (subsampleHorizontal == "1/4") {
			device.configSet(
				SAMSUNG_EVK_DVS, SAMSUNG_EVK_DVS_SUBSAMPLE_HORIZONTAL, SAMSUNG_EVK_DVS_SUBSAMPLE_HORIZONTAL_FOURTH);
		}
		else if (subsampleHorizontal == "1/8") {
			device.configSet(
				SAMSUNG_EVK_DVS, SAMSUNG_EVK_DVS_SUBSAMPLE_HORIZONTAL, SAMSUNG_EVK_DVS_SUBSAMPLE_HORIZONTAL_EIGHTH);
		}
		else {
			// No sub-sampling.
			device.configSet(
				SAMSUNG_EVK_DVS, SAMSUNG_EVK_DVS_SUBSAMPLE_HORIZONTAL, SAMSUNG_EVK_DVS_SUBSAMPLE_HORIZONTAL_NONE);
		}

		auto subsampleVertical = config.getString("subsampleVertical");
		if (subsampleVertical == "1/2") {
			device.configSet(
				SAMSUNG_EVK_DVS, SAMSUNG_EVK_DVS_SUBSAMPLE_VERTICAL, SAMSUNG_EVK_DVS_SUBSAMPLE_VERTICAL_HALF);
		}
		else if (subsampleVertical == "1/4") {
			device.configSet(
				SAMSUNG_EVK_DVS, SAMSUNG_EVK_DVS_SUBSAMPLE_VERTICAL, SAMSUNG_EVK_DVS_SUBSAMPLE_VERTICAL_FOURTH);
		}
		else if (subsampleVertical == "1/8") {
			device.configSet(
				SAMSUNG_EVK_DVS, SAMSUNG_EVK_DVS_SUBSAMPLE_VERTICAL, SAMSUNG_EVK_DVS_SUBSAMPLE_VERTICAL_EIGHTH);
		}
		else {
			// No sub-sampling.
			device.configSet(
				SAMSUNG_EVK_DVS, SAMSUNG_EVK_DVS_SUBSAMPLE_VERTICAL, SAMSUNG_EVK_DVS_SUBSAMPLE_VERTICAL_NONE);
		}

		device.configSet(SAMSUNG_EVK_DVS, SAMSUNG_EVK_DVS_DUAL_BINNING_ENABLE, config.getBool("dualBinning"));

		// TODO: add area blocking.

		auto externalTriggerMode = config.getString("externalTriggerMode");
		if (externalTriggerMode == "Single Frame Readout") {
			device.configSet(SAMSUNG_EVK_DVS, SAMSUNG_EVK_DVS_EXTERNAL_TRIGGER_MODE,
				SAMSUNG_EVK_DVS_EXTERNAL_TRIGGER_MODE_SINGLE_FRAME);
		}
		else {
			device.configSet(SAMSUNG_EVK_DVS, SAMSUNG_EVK_DVS_EXTERNAL_TRIGGER_MODE,
				SAMSUNG_EVK_DVS_EXTERNAL_TRIGGER_MODE_TIMESTAMP_RESET);
		}

		device.configSet(SAMSUNG_EVK_DVS, SAMSUNG_EVK_DVS_GLOBAL_RESET_ENABLE, config.getBool("globalReset"));
		device.configSet(
			SAMSUNG_EVK_DVS, SAMSUNG_EVK_DVS_GLOBAL_RESET_DURING_READOUT, config.getBool("globalResetDuringReadout"));
		device.configSet(SAMSUNG_EVK_DVS, SAMSUNG_EVK_DVS_GLOBAL_HOLD_ENABLE, config.getBool("globalHold"));
		device.configSet(SAMSUNG_EVK_DVS, SAMSUNG_EVK_DVS_FIXED_READ_TIME_ENABLE, config.getBool("fixedReadTime"));

		// TODO: add timing options.

		device.configSet(SAMSUNG_EVK_DVS_CROPPER, SAMSUNG_EVK_DVS_CROPPER_ENABLE, config.getBool("crop"));
		device.configSet(SAMSUNG_EVK_DVS_CROPPER, SAMSUNG_EVK_DVS_CROPPER_X_START_ADDRESS,
			static_cast<uint32_t>(config.getInt("cropStartX")));
		device.configSet(SAMSUNG_EVK_DVS_CROPPER, SAMSUNG_EVK_DVS_CROPPER_Y_START_ADDRESS,
			static_cast<uint32_t>(config.getInt("cropStartY")));
		device.configSet(SAMSUNG_EVK_DVS_CROPPER, SAMSUNG_EVK_DVS_CROPPER_X_END_ADDRESS,
			static_cast<uint32_t>(config.getInt("cropEndX")));
		device.configSet(SAMSUNG_EVK_DVS_CROPPER, SAMSUNG_EVK_DVS_CROPPER_Y_END_ADDRESS,
			static_cast<uint32_t>(config.getInt("cropEndY")));

		device.configSet(SAMSUNG_EVK_DVS_ACTIVITY_DECISION, SAMSUNG_EVK_DVS_ACTIVITY_DECISION_ENABLE,
			config.getBool("activityMonitor"));
		device.configSet(SAMSUNG_EVK_DVS_ACTIVITY_DECISION, SAMSUNG_EVK_DVS_ACTIVITY_DECISION_POS_THRESHOLD,
			static_cast<uint32_t>(config.getInt("activityPositiveThreshold")));
		device.configSet(SAMSUNG_EVK_DVS_ACTIVITY_DECISION, SAMSUNG_EVK_DVS_ACTIVITY_DECISION_NEG_THRESHOLD,
			static_cast<uint32_t>(config.getInt("activityNegativeThreshold")));
		device.configSet(SAMSUNG_EVK_DVS_ACTIVITY_DECISION, SAMSUNG_EVK_DVS_ACTIVITY_DECISION_DEC_RATE,
			static_cast<uint32_t>(config.getInt("activityDecrementRate")));
		device.configSet(SAMSUNG_EVK_DVS_ACTIVITY_DECISION, SAMSUNG_EVK_DVS_ACTIVITY_DECISION_DEC_TIME,
			static_cast<uint32_t>(config.getInt("activityDecrementTime")));
		device.configSet(SAMSUNG_EVK_DVS_ACTIVITY_DECISION, SAMSUNG_EVK_DVS_ACTIVITY_DECISION_POS_MAX_COUNT,
			static_cast<uint32_t>(config.getInt("activityPositiveMaxValue")));

		auto biasSensitivity = config.getString("biasSensitivity");
		if (biasSensitivity == "Very Low") {
			device.configSet(SAMSUNG_EVK_DVS_BIAS, SAMSUNG_EVK_DVS_BIAS_SIMPLE, SAMSUNG_EVK_DVS_BIAS_SIMPLE_VERY_LOW);
		}
		else if (biasSensitivity == "Low") {
			device.configSet(SAMSUNG_EVK_DVS_BIAS, SAMSUNG_EVK_DVS_BIAS_SIMPLE, SAMSUNG_EVK_DVS_BIAS_SIMPLE_LOW);
		}
		else if (biasSensitivity == "High") {
			device.configSet(SAMSUNG_EVK_DVS_BIAS, SAMSUNG_EVK_DVS_BIAS_SIMPLE, SAMSUNG_EVK_DVS_BIAS_SIMPLE_HIGH);
		}
		else if (biasSensitivity == "Very High") {
			device.configSet(SAMSUNG_EVK_DVS_BIAS, SAMSUNG_EVK_DVS_BIAS_SIMPLE, SAMSUNG_EVK_DVS_BIAS_SIMPLE_VERY_HIGH);
		}
		else {
			// Default biases.
			device.configSet(SAMSUNG_EVK_DVS_BIAS, SAMSUNG_EVK_DVS_BIAS_SIMPLE, SAMSUNG_EVK_DVS_BIAS_SIMPLE_DEFAULT);
		}
	}
};

registerModuleClass(samsungEVK)
