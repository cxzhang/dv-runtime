from test_framework._support import *
from test_framework._test_params import *


@davis_input
@output_hashes([["output0_hash", "98b66bba82125400b2107778f0339a5b9fcfa646e53c7c9034019b0183a6697f"],
                ["output1_hash", "dcc4628006730d58839032aad0a6af682e92dfea753e88ccc2479223334f82d5"],
                ["output2_hash", "c0001f6e35a4446eb735b87150d1a8e6a78748c7dc9ce3d2484de471b43fede3"]])
@output_written_data_sizes([['output0_written_data_size', 621249606], ['output1_written_data_size', 609870],
                            ['output2_written_data_size', 23613526]])
def test_04_InputOutput(self):
    output_inputs = ["events", "imu", "frames"]
    test_io_with_no_module(self, output_inputs)
