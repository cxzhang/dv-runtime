from test_framework._support import *
from test_framework._test_params import *


@davis_input
@output_hash("ded5614de1bff93a7a36bfe33dfc1b7c138b0550be099285af876ce1943c0a01")
def test_09_02_FrameContrastHistogram(self):
    module_parameters = [["contrastAlgorithm", "histogram_equalization"]]
    module = dv_module("dv_frame_contrast", [["frames", "input[frames]"]], ["frames"], config_options=module_parameters)
    test_modules_with_io(self, [module])
