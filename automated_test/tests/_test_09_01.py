from test_framework._support import *
from test_framework._test_params import *


@davis_input
@output_hash("ece40ef6b5a9f82ac8fb7dd2c605f734f8972e96e25c1f87a3163aa4960ec8c1")
def test_09_01_FrameContrastNormalisation(self):
    module = dv_module("dv_frame_contrast", [["frames", "input[frames]"]], ["frames"])
    test_modules_with_io(self, [module])
