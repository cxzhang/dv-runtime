from test_framework._support import *
from test_framework._test_params import *


@davis_input
@output_hash("623aa4b0f57d42e5c9b7f1b564dd32b18b2ea23cfc5557155c03d1c561426e8f")
def test_07_01_CropScaleEvents(self):
    module_parameters = [["cropOffsetX", "50"], ["cropWidth", "100"], ["cropOffsetY", "50"], ["cropHeight", "100"],
                         ["resizeWidth", "200"], ["resizeHeight", "200"], ["resize", "true"]]
    module = dv_module("dv_crop_scale", [["events", "input[events]"]], ["events"], config_options=module_parameters)
    test_modules_with_io(self, [module])
