from test_framework._support import *
from test_framework._test_params import *


@dvxplorer_input
@output_hash("680ae57b5a7f01beeae0d0b96acd82efa800780259058048bbed4c78c13ef033")
def test_05_02_accumulatorLinear(self):
    module_parameters = [["decayFunction", "Linear"]]
    module = dv_module("dv_accumulator", [["events", "input[events]"]], ["frames"], config_options=module_parameters)
    test_modules_with_io(self, [module])
