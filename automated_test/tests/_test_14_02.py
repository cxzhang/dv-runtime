from test_framework._support import *
from test_framework._test_params import *


@aedat3_0_input
@output_hashes([["output0_hash", "872ede1858bdd8569a7703eec16affcd051230339caa4a214660367b413c3486"],
                ["output1_hash", "b0fac5239ecfd7b6032f64d6f7aa72c62b5f33c298e89a4605904c7aaf7a12f2"],
                ["output2_hash", "8384dcccc74ebbe8adf5a822a7a32af06bc1e6cef3630bafd31b697a0064a29f"]])
@output_written_data_sizes([["output0_written_data_size", 383013710], ["output1_written_data_size", 8236398],
                            ["output2_written_data_size", 25325526]])
def test_14_02_converter_aedat3(self):
    module_parameters = [["file", test_params["input_file"]]]
    module = dv_module("converter",
                       outputs=["events", "imu", "frames"],
                       config_options=module_parameters,
                       library="dv_converter")
    test_modules_with_outputs(self, [module])
