from test_framework._support import *
from test_framework._test_params import *


@dvxplorer_input
@output_hash("331bc6cf66b2f2da63ae3a7c2cd9c6952c9a37e68e72db4f448d2d2efde288fe")
def test_05_03_accumulatorStep(self):
    module_parameters = [["decayFunction", "Step"]]
    module = dv_module("dv_accumulator", [["events", "input[events]"]], ["frames"], config_options=module_parameters)
    test_modules_with_io(self, [module])
