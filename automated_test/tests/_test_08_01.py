from test_framework._support import *
from test_framework._test_params import *


@davis_input
@output_hash("8874fc16e85972118ce94814b6e598e319565edf4ce8b4bb671d5bf71ed61ca7")
def test_08_01_FlipRotateEventsHVD(self):
    module_parameters = [["degreeOfRotation", "90"], ["eventFlipVertically", "true"], ["eventFlipHorizontally", "true"]]
    module = dv_module("dv_flip_rotate", [["events", "input[events]"]], ["events"], config_options=module_parameters)
    test_modules_with_io(self, [module])
