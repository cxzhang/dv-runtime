from test_framework._support import *
from test_framework._test_params import *


@aedat2_0_input
@output_hashes([["output0_hash", "170478c3a3299704a1dd05ab6c4b28dafd1bd4b2a716306f6737d74fda3ea5c2"],
                ["output1_hash", "dedb439f220be08fa28a19975bcbda758c0826f0d170eba2b8474cc8329b8606"],
                ["output2_hash", "f150efaeb36b6fe01e434720112400e1a7e89df09f39bbd9f8380ae517602f58"]])
@output_written_data_sizes([["output0_written_data_size", 7492814], ["output1_written_data_size", 212734],
                            ["output2_written_data_size", 930]])
def test_14_01_converter_aedat2_0(self):
    run_test(self)


@aedat2_1_input
@output_hashes([["output0_hash", "460fface243e455909b1b81a640150760ec4fc21ae761376d8d112fd7f305141"],
                ["output1_hash", "a8bce48a3aae6a98902cb7c9cb9bab888e08fbaf13d8dcf6f6d0c83a507a6919"],
                ["output2_hash", "bbf89c065d93303434b7eca4c3a9b02b38a7c3723a58a1165561353d37355c12"]])
@output_written_data_sizes([["output0_written_data_size", 173156070], ["output1_written_data_size", 802],
                            ["output2_written_data_size", 906]])
def test_14_01_converter_aedat2_1(self):
    run_test(self)


@aedat2_2_input
@output_hashes([["output0_hash", "501f568e0b784604dc6032587245e3150ec79519acfad489ea563b1c86f470f9"],
                ["output1_hash", "cb78ff87f7bd9034e8353f0f96b27f52114cd9b0b416fd34189dc69e7ff60e8c"],
                ["output2_hash", "00febec55b7b1631f26b13bbf1de2d272fc836d60328d01bf2fea7756f0e4b2e"]])
@output_written_data_sizes([["output0_written_data_size", 3984390], ["output1_written_data_size", 214638],
                            ["output2_written_data_size", 1215134]])
def test_14_01_converter_aedat2_2(self):
    run_test(self)


@aedat2_3_input
@output_hashes([["output0_hash", "13461c4e64a5274e6df77857b758921e13ce1a2b318ff096f1307c538e10c359"],
                ["output1_hash", "2d13287d8aba49ca1692606286ef855056309825ce107ba52528b6bb7e304c94"],
                ["output2_hash", "80915cb0cd8f632c42334bcd0feb4e4e35d6b18b01b270bc796f7628b609b60e"]])
@output_written_data_sizes([["output0_written_data_size", 767126], ["output1_written_data_size", 59430],
                            ["output2_written_data_size", 906]])
def test_14_01_converter_aedat2_3(self):
    run_test(self)


@aedat2_4_input
@output_hashes([["output0_hash", "587c73e855db4dd8e20c25968ed2dc04974b8b32376aec4a58d2b559448c95ea"],
                ["output1_hash", "f757c2e3c595ee805fab839ba3bb352f24088d395d568bf506b6376bccc6337c"],
                ["output2_hash", "80915cb0cd8f632c42334bcd0feb4e4e35d6b18b01b270bc796f7628b609b60e"]])
@output_written_data_sizes([["output0_written_data_size", 3109142], ["output1_written_data_size", 58366],
                            ["output2_written_data_size", 906]])
def test_14_01_converter_aedat2_4(self):
    run_test(self)


@aedat2_5_input
@output_hashes([["output0_hash", "1f20fdb9908a1938701bf5c4f2a97765128614af251accd1734edd15f1a31710"],
                ["output1_hash", "c866277a3dac0d4d8a93410a73627643dc469fd3c81e7719937eac225599e883"],
                ["output2_hash", "34409a8d30b3738ccbd93d661ac49c6a59f9359788614307634392456b87d5b8"]])
@output_written_data_sizes([["output0_written_data_size", 60579350], ["output1_written_data_size", 850],
                            ["output2_written_data_size", 954]])
def test_14_01_converter_aedat2_5(self):
    run_test(self)


@aedat2_6_input
@output_hashes([["output0_hash", "b67c622ca342f81a755397b9086bfdbd92f9ef7a8fa107bf4aa06555cf57d5e2"],
                ["output1_hash", "ca0b2a3916a4cd22036a8b25cd100edf6118310ad1b44cf978604a323a074c07"],
                ["output2_hash", "bef6bacb38c60a3edffef3e969c182e2e4e79be4ff902e3cfb6a9c487bb40381"]])
@output_written_data_sizes([["output0_written_data_size", 84284166], ["output1_written_data_size", 802],
                            ["output2_written_data_size", 906]])
def test_14_01_converter_aedat2_6(self):
    run_test(self)


@aedat2_7_input
@output_hashes([["output0_hash", "e55ede9cafdbcc82807adb81b2968ab66201aa9b6b9d81a93a9811d0fe92ca4d"],
                ["output1_hash", "d4d347d0fc81b35752c10383ed0ecc6f54eae297d138bfe8426c49e291d6fc44"],
                ["output2_hash", "656b83bb810dd573b3efe98f459a002822dc781d94e08c336645d0f069f5d165"]])
@output_written_data_sizes([["output0_written_data_size", 21861358], ["output1_written_data_size", 794],
                            ["output2_written_data_size", 898]])
def test_14_01_converter_aedat2_7(self):
    run_test(self)


@aedat2_8_input
@output_hashes([["output0_hash", "2962d7a305808753eae434ce8ff7acb34a3881381d460814d00aa73897fe9cdd"],
                ["output1_hash", "b5dd51fb82f6fb682727d50ffd4de4b79fd2a8596fd2c0a55531839ebd6bda0e"],
                ["output2_hash", "80915cb0cd8f632c42334bcd0feb4e4e35d6b18b01b270bc796f7628b609b60e"]])
@output_written_data_sizes([["output0_written_data_size", 2355166], ["output1_written_data_size", 58606],
                            ["output2_written_data_size", 906]])
def test_14_01_converter_aedat2_8(self):
    run_test(self)


@aedat2_9_input
@output_hashes([["output0_hash", "308221f3d63bfb2c3fd2ca1ed0af0b66ea77c5e2807a4995bb3f8f1317fed628"],
                ["output1_hash", "bb297d12e093d026f8aad759d554bcdf49bd28f87eb7184202e83077c890a315"],
                ["output2_hash", "319ff61f5923fbdbb41fc76d29ec409fb83f5d4ffa02839ad06f0325607a698e"]])
@output_written_data_sizes([["output0_written_data_size", 802878], ["output1_written_data_size", 786],
                            ["output2_written_data_size", 890]])
def test_14_01_converter_aedat2_9(self):
    run_test(self)


def run_test(self):
    module_parameters = [["file", test_params["input_file"]]]
    module = dv_module("converter",
                       outputs=["events", "imu", "frames"],
                       config_options=module_parameters,
                       library="dv_converter")
    test_modules_with_outputs(self, [module])
