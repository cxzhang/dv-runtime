from test_framework._support import *
from test_framework._test_params import *


@dvxplorer_input
@output_hash("17bbffed1685e3552db5584194f1458d8d1917a57098cef52304c9658ebc04d4")
def test_05_04_accumulatorNone(self):
    module_parameters = [["decayFunction", "None"]]
    module = dv_module("dv_accumulator", [["events", "input[events]"]], ["frames"], config_options=module_parameters)
    test_modules_with_io(self, [module])
